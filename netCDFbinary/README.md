# netCDFbinary
netCDFbinary is a python package for converting MITgcm binary model output `.data` files to NetCDF `.nc` files. The variables to be included in the netcdf files are specified as command line arguments.

See the documentation in the docs folder or online on [readthedocs](https://gcmpy.readthedocs.io/en/latest/netCDFbinary/)
