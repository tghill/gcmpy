import glob

iters = sorted([int(f.split('_')[1].split('.')[0]) for f in glob.glob('*.nc')])
#plotiters = (0, 165600)

GLOBAL_GIF_ARGS = { 'gif_folder_name' : 'GIF',
                    'image_folder_name':'PNG',
                    'fps' :             4,
                    'plot_type' :       'gs',
                    'iters' :           iters[:310:2],
                    'data_path' :       '.',
#                    'dpi' :             600,
#                    'cmap' :            'cmocean/thermal',
}

vmin = -1
vmax = 5
scale = 10
stride = 10
zstride = 5
ARGS3D = [
                 { 'var' :              'T',
                   'movie_name' :       'hydro_comp_stc_surface_T.gif',
                   'image_name' :       'hydro_comp_stc_surface_T_.png',
                   'cut_var' :          'z',
                   'vmin' :             vmin,
                   'vmax' :             vmax,
                   'cut_val' :          0,
                   'velocity_field' :   False,
                   'stride' :           stride,
                   'zstride' :          zstride,
                   'scale' :            scale,
                   'cmap' :             'cmocean/thermal'},
                 { 'var' :              'T',
                   'movie_name' :       'hydro_comp_stc_x_3000_T.gif',
                   'image_name' :       'hydro_comp_stc_x_3000_T_.png',
                   'cut_var' :          'x',
                   'vmin' :             vmin,
                   'vmax' :             vmax,
                   'cut_val' :          3000,
                   'velocity_field' :   False,
                   'stride' :           stride,
                   'zstride' :          zstride,
                   'scale' :            scale,
                    'cmap' :             'cmocean/thermal'},
#                 { 'var' :              'T',
#                   'movie_name' :       'hydro_comp_stc_y_1500_T.gif',
#                   'image_name' :       'hydro_comp_stc_y_1500_T_.png',
#                   'cut_var' :          'y',
#                   'vmin' :             vmin,
#                   'vmax' :             vmax,
#                   'cut_val' :          1500,
#                   'velocity_field' :   True,
#                   'stride' :           stride,
#                   'zstride' :          zstride,
#                   'scale' :            scale}
#               {   'var' : 'W',
#                    'movie_name': 'hydro_comp_stc_z_25_W.gif',
#                    'image_name': 'hydro_comp_stc_z_25_W_.png',
#                    'cut_var': 'z',
#                    'cut_val' : 2,
#                    'velocity_field': False,
#                    'cmap' : 'cmocean.cm/delta',
#                    'vmin' : -1.e-4,
#                    'vmax' : 1.e-4,
#                    },

#                {   'var' : 'V',
#                    'movie_name': 'hydro_comp_stc_z_25_V.gif',
#                    'image_name': 'hydro_comp_stc_z_25_V_.png',
#                    'cut_var': 'z',
#                    'cut_val' : 2,
#                    'velocity_field': False,
#                    'cmap' : 'cmocean.cm/delta',
#                    'vmin' : -0.05,
#                    'vmax' : 0.05,
#                    }
]

ARGS2D = [
                 { 'var' :              'SI_Fract',
                   'movie_name' :       'hydro_comp_stc_surface_ice.gif',
                   'image_name' :       'hydro_comp_stc_surface_ice_.png',
                   'vmin' :             0,
                   'vmax' :             1,
                   'ice_velocity_field':False,
                   'stride' :           stride},

                { 'var' :              'SIheff',
                  'movie_name' :       'hydro_comp_stc_surface_ice_thick.gif',
                  'image_name' :       'hydro_comp_stc_surface_ice_thick_.png',
                  'vmin' :             0,
                  'vmax' :             1.0,
                  'ice_velocity_field':False,
                  'stride' :           stride},

                { 'var' :              'SIqnet',
                  'movie_name' :       'hydro_comp_stc_surface_SIflx.gif',
                  'image_name' :       'hydro_comp_stc_surface_SIflx_.png',
                  'ice_velocity_field':False,
                  'vmin' : 0,
                  'vmax' : 50,
                  'cmap' : 'cmocean/thermal'
                  }
 ]

