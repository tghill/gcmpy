"""

    Example file specifying parameters for MITgcm animation

"""

# Arguments for all the figures
GLOBAL_GIF_ARGS = { 'start_time' :      0,
                    'sec_per_iter' :    60.0,
                    'vmin' :            16,
                    'vmax' :            21,
                    'gif_folder_name' : 'GIF',
                    'image_folder_name':'PNG',
                    'bathy_file_name' : 'Bathymetry.bin',
                    'fps' :             4,
                    'plot_type' :       'gs',
}


ARGS3D = [
                 { 'var' :              'T'
                   'movie_name' :       'surface_T.gif',
                   'image_name' :       'surface_T_.png',
                   'cut_var' :          'z',
                   'cut_val' :          0 },

                 { 'var' :              'T'
                   'movie_name' :       'x_3000_T.gif',
                   'image_name' :       'x_3000_T_.png',
                   'cut_var' :          'x',
                   'cut_val' :          3000 },

                 { 'var' :              'T'
                   'movie_name' :       'y_1500_T.gif',
                   'image_name' :       'surface_T_.png',
                   'cut_var' :          'y',
                   'cut_val' :          1500 }
       ]


ARGS2D = [
                 { 'var' :              'ice_fract'
                   'movie_name' :       'surface_ice.gif',
                   'image_name' :       'surface_ice_.png'}
       ]
