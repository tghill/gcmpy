"""
    ___main__.py for MITgcmpp package.

    Lets the package be called as

    $ python -m MITgcmpp optfile


    Defines the command line interface.

"""

import importlib
from importlib import machinery

import argparse
import os

from . import twodimensionalgif
from . import threedimensionalgif

import f90nml

def import_from_path(path):
    """
    import_from_path imports a module from the full file path.

    Function returns the module. Intended usage would be,

        newmod = import_from_path('../path/to/module')

        foo = newmod.function()

    Input:
     *  path, the full file path to the module

    Returns:
     *  A module object corresponding to the python module at the input file
            path
    """
    moddir, modfile = os.path.split(path)
    modname = os.path.splitext(modfile)[0]
    module = machinery.SourceFileLoader(modname, path).load_module()
    return module

def get_data(path):
    """get_data gets relevant parameters from the 'data' model config file.

    Inputs:
     *  path: path (relative or abs) to the data file used to configure
                the model

    Returns:
     *  dictionary containing keys 'sec_per_iter', 'start_time'.
    """
    time_nml = 'PARM03'
    parser = f90nml.Parser()
    parser.comment_tokens += '#'
    data = parser.read(os.path.join(path, 'data'))
    return     {   'sec_per_iter' :    data[time_nml]['deltaT'],
                   'start_time'   :    data[time_nml]['startTime']
               }

# create parser, and take command line arguments
parser = argparse.ArgumentParser()
parser.add_argument('optfile', help = 'Animation options file')
args = parser.parse_args()
optmodule = import_from_path(os.path.abspath(args.optfile))

# Process the module

# Make 3D plots
if hasattr(optmodule, 'ARGS3D'):
    for argspec3d in optmodule.ARGS3D:
        if hasattr(optmodule, 'GLOBAL_GIF_ARGS'):
            args = optmodule.GLOBAL_GIF_ARGS.copy()
        else:
            args = {}
        args.update(argspec3d)
        if 'data_path' in args:
            args.update(get_data(os.path.abspath(args['data_path'])))
        threedimensionalgif.makeanimate(args)

# Make 2D plots
if hasattr(optmodule, 'ARGS2D'):
    for argspec2d in optmodule.ARGS2D:
        if hasattr(optmodule, 'GLOBAL_GIF_ARGS'):
            args = optmodule.GLOBAL_GIF_ARGS.copy()
        else:
            args = {}
        args.update(argspec2d)
        if 'data_path' in args:
            args.update(get_data(os.path.abspath(args['data_path'])))
        twodimensionalgif.makeanimate(args)
