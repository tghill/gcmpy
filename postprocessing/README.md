# README for python post-processing routines.
Post processing routines in python for MITgcm output data.

See the documentation on the docs folder or online on [readthedocs](https://gcmpy.readthedocs.io/en/latest/postprocessing/)
