"""

fluxes.py

Plot available heat fluxes from the SEAICE package.

    Panels plotted are (left to right across rows):

        (1)     Rate of change of total internal energy of water
        (2)     Rate of change of total internal energy of water per unit area
        (3)     SIqnet: Total heat flux out of the water surface.
                        For ice covered areas, flux is water to ice.
                        For open water, flux is water to atmosphere.
        (4)     SIatmQnt: Total atmospheric heat flux. For ice covered
                        areas, heat flux from ice to atmosphere. For
                        open water, heat flux from ocean to atmosphere.
        (5)     Ice covered part of SIqnet. Total heat flux from water
                        into ice, averaged over the whole domain.
        (6)     Ice covered part of SIatmQnt. Total heat flux from ice
                        into atmosphere, averaged over the whole domain.
        (7)     Rate of change of total internal energy of water per unit area +
                        net heat flux out of the water. This should theoretically
                        be identically zero, but the rate of change plotted in (2)
                        is computed numerically as an average between files. Some
                        small numerical error is expected.
        (8)     EXFhs:  Sensible heat flux into water, positive increases temperature
        (9)     EXFhl:  Latent heat flux into water, positive increases temperature

Panels (5) and (6) use a threshold area fraction of 1.e-5 to delineate ice-covered
and open water. This works well for simulations with a hard boundary between ice-
covered and open water. This value might not be suitable for all possible cases.


Uses standard command line interface for single-run plots.

"""

import argparse
import os

import numpy as np
import netCDF4 as nc

import matplotlib
matplotlib.use('Agg')

from matplotlib import pyplot as plt

from . import modeldata
from . import find
from . import interface
from . import timeutils

# Constant: specific heat capacity of water (J / (kg C) )
c = 4186.0

# Constant: Density of water at 4C
rhoref = 1.e3

hicemin = 1.e-3

dir = ''
def main(figname, start, stop, step, time_unit = ''):
    filedata = find.all(dir, start = start, stop = stop, step = step)
    mp = modeldata.getdata(data = os.path.join(dir, 'data'),
                                  datapkg = os.path.join(dir, 'data.pkg'))
    nfiles = len(filedata.files)
    shape = (nfiles,)
    deltaQ = np.zeros(shape)

    data0 = nc.Dataset(filedata.files[0])
    T0 = np.array(data0['T'])

    # Initialize data arrays
    area     = np.zeros(shape)      # Ice-covered are
    SIqnet   = np.zeros(shape)      # SIqnet (heat flux out of water)
    SIatmQnt = np.zeros(shape)      # SIatmQnt (heat flux to atmosphere)
    EXFhs    = np.zeros(shape)      # Sensible heat flux into water
    EXFhl    = np.zeros(shape)      # Latent heat flux into water
    SIqneti  = np.zeros(shape)      # Water to ice heat flux
    SIqneto  = np.zeros(shape)       # Water to atmosphere heat flux
    SIatmqnti= np.zeros(shape)      # Ice to atmosphere heat flux
    SIatmqnto= np.zeros(shape)      # Water to atmosphere heat flux
    # SIqice   = np.zeros(shape)      # Net heiat flux into ice (to melt or form ice)
    SIheff   = np.zeros(shape)      # Effective ice thickness
    totalarea = mp.dx * mp.nx * mp.dy * mp.ny
    print('Total area:', totalarea)
    for j,file in enumerate(filedata.files):
        print("Plotting file %s" % file)
        data = nc.Dataset(file)

        # Compute the change in internal energy
        T1 = np.array(data['T'])
        dT = T1 - T0
        try:
            rho = rhoref + np.array(data['Rho'])
        except:
            rho = rhoref
        deltaQ[j] = c * np.sum(rho * mp.dx * mp.dy * mp.dz * dT)

        try:
            areafract = data['SI_Fract'][:]
        except:
            try:
                areafract = data['SIarea'][:]
            except:
                raise
        dA = np.sum(areafract * mp.dx * mp.dy)
        area[j] = dA

        # Compute mean quantities
        siqnet = data['SIqnet'][:]
        SIqnet[j] = siqnet.mean()
        EXFhs[j] = data['EXFhs'][:].mean()
        EXFhl[j] = data['EXFhl'][:].mean()
        SIatmQnt[j] = data['SIatmQnt'][:].mean()
        SIheff[j] = data['SIheff'][:].mean()

        # Compute ice-covered and open-water only fluxes
        siqneti = siqnet.copy()
        siqneti[areafract < hicemin] = 0
        SIqneti[j] = siqneti.mean()

        siatmqnti = data['SIatmQnt'][:]
        siatmqnti[areafract < hicemin] = 0
        SIatmqnti[j] = siatmqnti.mean()

        siatmqnto = data['SIatmQnt'][:]
        siatmqnto[areafract > hicemin] = 0
        SIatmqnto[j] = np.array(siatmqnto).mean()

        SIqneto[j] = SIatmQnt[j] - SIatmqnti[j]
        # Net heat flux into ice
        # SIqice[j] = SIqnet[j] - SIatmQnt[j]

    times = mp.deltaT * np.array(filedata.iters)
    rate = np.array([(deltaQ[i+1] - deltaQ[i]) / (times[i+1] - times[i]) for i in range(len(filedata.files)-1)])

    mask = np.isnan(deltaQ)
    rate = rate[~mask[:-1]]
    times = times[~mask]
    area = area[~mask]
    SIqnet = SIqnet[~mask]
    EXFhl = EXFhl[~mask]
    EXFhs = EXFhs[~mask]

    # SIqneti is all zero for some reason; compute what it shoud be
#    SIqneti = SIqnet - SIqneto

    # Re-scale time array
    times, xlab = timeutils.convert(times, xlab)

    fig, axs  = plt.subplots(nrows = 3, ncols = 4, figsize = (12, 8), sharex = True)
    axes = np.ravel(axs)
    qp = axes[0].plot(times[:len(rate)], rate)
    axes[0].set_title('Rate of change of internal energy')
    axes[0].set_ylabel('dQ/dt (W)')

    axes[1].plot(times[:len(rate)], rate / totalarea)
    axes[1].set_title('Mean rate of change of Q per area')
    axes[1].set_ylabel('dQ/dt/A (W/m^2)')

    axes[2].plot(times[1:], SIqnet[1:])
    axes[2].set_title('Mean heat flux out of water')
    axes[2].set_ylabel('SIqnet (W/m^2)')

    axes[3].plot(times[1:], SIatmQnt[1:])
    axes[3].set_title('Mean heat flux to atmosphere')
    axes[3].set_ylabel('SIatmQnt (W/m^2)')

    axes[4].plot(times[1:], SIqneti[1:])
    axes[4].set_title('Mean water to ice flux')
    axes[4].set_ylabel("SIqnet ice covered (W/m^2)")

    axes[5].plot(times[1:], SIatmqnti[1:])
    axes[5].set_title('Mean ice to atmosphere flux')
    axes[5].set_ylabel('SIatmQnet ice covered (W/m^2)')

    axes[6].plot(times[1:len(rate)],SIqnet[1:len(rate)] + rate[1:] / totalarea)
    axes[6].set_ylabel('SIqnet + dQ/dt/A (W/m^2)')
    axes[6].set_title('Mean flux of system energy')

    axes[7].plot(times, EXFhs)
    axes[7].set_ylabel('EXFhs (W/m^2)')
    axes[7].set_title('Sensible heat flux to water')

    axes[8].plot(times, EXFhl)
    axes[8].set_ylabel('EXFhl (W/m^2)')
    axes[8].set_title('Latent heat flux to water')

    axes[9].plot(times, SIqneto)
    axes[9].set_title('Water to atmosphere flux')
    axes[9].set_ylabel('SIqneto (W/m^2)')

    for ax in axs[-1, :]:
        ax.set_xlabel(xlab)

    for ax in axes:
        ax.grid()

#    fig.subplots_adjust(hspace = 0.02, wspace = 0.2)
    plt.tight_layout()
    fig.savefig(figname, dpi = 600)

if __name__ == '__main__':
    parser = interface.single()
    main(args.file, args.start, args.stop, args.step, args.time_unit)
