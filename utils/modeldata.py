"""
    module modeldata.py

    Module for reading fortran namelist (data file) into python
    to set model data (temperature, bathymetry, etc.) consistent
    with the model grids.

    Fucntion getdata takes the path to the data files as optional keyword
    arguments, and returns a namedtuple object with attributes

                'dx',
                'dy',
                'dz',
                'nx',
                'ny',
                'nz',
                'startTime',
                'endTime',
                'deltaT'
                'delX',
                'delY',
                'delZ'

"""

import collections

import f90nml

datafields = [  'dx',
                'dy',
                'dz',
                'nx',
                'ny',
                'nz',
                'startTime',
                'endTime',
                'deltaT',
                'delX',
                'delY',
                'delZ'
            ]


def getdata(data = 'data', datapkg = 'data.pkg',
                    datadiagnostics = None):
    # Parse package list
    parser = f90nml.Parser()
    parser.comment_tokens += '#'
    datapkgnml = parser.read(datapkg)
    if datadiagnostics:
        datadiagnml = f90nml.read(datadiagnostics)
    else:
        datadiagnostics = {}
    datanml = f90nml.read(data)

    # diag is a flag specifying whether Diagonistics package was used
    diag = True if 'useDIAG' in datapkgnml and datapkgnml['useDIAG']==True else False

    delX = datanml['PARM04']['delX']
    delY = datanml['PARM04']['delY']
    delZ = datanml['PARM04']['delZ']

    nx = len(delX)
    ny = len(delY)
    nz = len(delZ)

    # This assumes evenly spaced data. Figure out how to avoid this assumption
    dx = delX[0]
    dy = delY[0]
    dz = delZ[0]

    # Add conditional if useDIAG=.TRUE. in data.pkg -- look for time data there
    start_time = datanml['PARM03']['startTime']
    end_time = datanml['PARM03']['endTime']
    sec_per_iter = datanml['PARM03']['deltaT']
    sec_per_file = datanml['PARM03']['monitorFreq']

    ModelData = collections.namedtuple('ModelData', datafields)

    modeldata = ModelData(dx, dy, dz, nx, ny, nz, start_time,
                            end_time, sec_per_iter, delX,
                            delY, delZ)
    return(modeldata)

if __name__ == "__main__":
    getdata()
