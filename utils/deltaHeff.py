"""

deltaHeff compares the mean effective ice thickness between model runs.

The command line interface adds the optional argument [--inset N]. Passing
this argument with an integer N includes an inset axis into the main axis
for the first N seconds. Default value is None, which does not include an inset

"""

import os

import numpy as np
import netCDF4 as nc

import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
from mpl_toolkits.axes_grid1 import inset_locator

from . import modeldata
from . import find
from . import interface
from . import timeutils

field = 'SIheff'
ylabel = 'Thickness (m)'
title = 'Mean ice thickness'

def makeplot(figname, dirs, labels, start, stop, step, include_delta = False, timeunit = '', inset = None):
    filedata = [find.all(dir, step = step, start = start, stop = stop) for dir in dirs]
    mp = [modeldata.getdata(data = os.path.join(dir, 'data'), datapkg = os.path.join(dir, 'data.pkg')) for dir in dirs]

    nfiles = len(filedata[0].files)

    delta = [np.zeros(np.shape(fd.iters)) for fd in filedata]

    for j in range(len(delta)):
        for i in range(len(filedata[j].files)):
            print('Plotting file %s' % os.path.join(dirs[j], filedata[j].files[i]))

            try:
                data = nc.Dataset(os.path.join(dirs[j], filedata[j].files[i]), 'r')
                delta[j][i] = data[field][:].mean()
            except:
                print('Error reading file %s' % os.path.join(dirs[j], filedata[j].files[i]))
                delta[j][i] = np.nan

    times = [mp[j].deltaT * np.array(fd.iters) for j,fd in enumerate(filedata)]
    times, xlab = timeutils.convert(times, timeunit)

    if include_delta:
        fig, (ax1, ax2) = plt.subplots(figsize = (6, 6), sharex = True, nrows = 2)
        ax2.plot(times[0], delta[-1] - delta[0])
        ax2.grid()
        ax2.set_xlabel(xlab)
        ax2.set_ylabel('Delta %s' % ylabel)
        ax2.set_title('Delta %s' % title)

    else:
        fig, ax1 = plt.subplots()
        ax1.set_xlabel(xlab)

    if inset:
        axins = fig.add_axes((0.1375, 0.465, 0.275, 0.275))
        axins.grid()
        axins.tick_params(axis = 'y', left = False, labelleft = False)

    for j, label in enumerate(labels):
        ax1.plot(times[j], delta[j], label = label)
        if inset:
            inset_index = np.where(times[j] == inset)[0][0]
            axins.plot(times[j][:inset_index], delta[j][:inset_index])

    ax1.grid()
    ax1.legend(loc = 'upper left', fontsize = 8, labelspacing = 0.2)

    ax1.set_ylabel(ylabel)
    ax1.set_title(title)

    ax1.set_xlim(0, ax1.get_xlim()[1])
    ax1.set_ylim(0, ax1.get_ylim()[1])
#    plt.tight_layout()

    fig.savefig(figname, dpi = 600)

def main():
    parser = interface.ArgumentParser()
    parser.add_argument('--inset', help = 'Show inset of first N seconds', default = None, type = float)
    args = parser.parse_args()

    makeplot(figname = args.file, dirs = args.dirs, labels = args.labels, step = args.step,
            include_delta = args.include_delta, start = args.start, stop = args.stop,
            timeunit = args.time_unit, inset = args.inset)

if __name__ == '__main__':
    main()
