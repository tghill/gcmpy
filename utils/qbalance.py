"""

qbalance.py

Plot and compare heat fluxes out of ocean to the rate of change
of internal energy.

Internal energy is computed as

    Q = c * rho * (dx*dy*dz) * dT

Where dT is the change in temperature from the initial state;
dx, dy, dz are the cell sizes.

The rate of change is computed as a first order difference

    dQ/dt ~ (Q(T_{i+1} - T_i) / (T_{i+1} - T_i)


This computed rate of change is compared to the variable SIqnet: Net
heat flux out of the ocean. This is heat flux from ocean to ice for
ice-covered areas, and heat flux from ocean to atmosphere for open
water regions.

Theoretically, dQ/dt = -SIqnet identically. Therefore, plotting
dQ/dt + SIqnet = 0 identically. In practice, some numerical error
should be expected based on the derivative scheme.


"""

import argparse
import os

import numpy as np
import netCDF4 as nc

import matplotlib
matplotlib.use('Agg')

from matplotlib import pyplot as plt

from . import modeldata
from . import find
from . import interface
from . import timeutils

# Constant: specific heat capacity of water (J / (kg C) )
c = 4186.0

# Constant: Density of water at 4C
rhoref = 1.e3
dir = ''
def makeplot(figname, start, stop, step, time_unit = ''):
    filedata = find.all(dir)
    mp = modeldata.getdata(data = os.path.join(dir, 'data'),
                                  datapkg = os.path.join(dir, 'data.pkg'))
    nfiles = len(filedata.files)
    shape = (nfiles,)
    deltaQ = np.zeros(shape)

    data0 = nc.Dataset(filedata.files[0])
    T0 = np.array(data0['T'])

    # Initialize data arrays
    area     = np.zeros(shape)
    SIqnet   = np.zeros(shape)

    totalarea = mp.dx * mp.nx * mp.dy * mp.ny
    for j,file in enumerate(filedata.files):
        print("Plotting file %s" % file)
        data = nc.Dataset(file)
        T1 = np.array(data['T'])
        dT = T1 - T0

        try:
            rho = rhoref + np.array(data['Rho'])
        except:
            rho = rhoref

        deltaQ[j] = c * np.sum(rho * mp.dx * mp.dy * mp.dz * dT)

        areafract = data['SI_Fract'][:]
        dA = np.sum(areafract * mp.dx * mp.dy)
        area[j] = dA

        siqnet = data['SIqnet'][:]
        SIqnet[j] = siqnet.mean()
    times = mp.deltaT * np.array(filedata.iters)
    times, xlab = timeutils.convert(times, time_unit)

    rate = np.array([(deltaQ[i+1] - deltaQ[i]) / (times[i+1] - times[i]) for i in range(len(filedata.files)-1)])

    mask = np.isnan(deltaQ)
    rate = rate[~mask[:-1]]
    times = times[~mask]
    area = area[~mask]
    SIqnet = SIqnet[~mask]

    # SIqneti is all zero for some reason; compute what it shoud be
#    SIqneti = SIqnet - SIqneto

    fig, axs  = plt.subplots(nrows = 2, ncols = 2, figsize = (10, 8), sharex = True)
    axes = np.ravel(axs)
    qp = axes[0].plot(times[:len(rate)], rate)
    axes[0].set_ylabel('dQ/dt (W)')
    axes[0].set_title('Rate of change internal energy')

    axes[1].plot(times[:len(rate)], rate / totalarea)
    axes[1].set_ylabel('dQ/dt/A (W/m^2)')
    axes[1].set_title('Rate of change per area internal energy')

    axes[2].plot(times[1:], SIqnet[1:])
    axes[2].set_ylabel('SIqnet (W/m^2)')
    axes[2].set_title('Ice/atmosphere to ocean heat flux')

    axes[3].plot(times[1:len(rate)],SIqnet[1:len(rate)] + rate[1:] / totalarea)
    axes[3].set_ylabel("SIqnet + dQ/dt/A (W/m^2)")
    axes[3].set_title('Rate of change internal energy + heat flux')

    for ax in axs[-1, :]:
        ax.set_xlabel(xlab)

    for ax in axes:
        ax.grid()

#    fig.subplots_adjust(hspace = 0.02, wspace = 0.2)
    plt.tight_layout()
    fig.savefig(figname, dpi = 600)

    print(area[0])
    print(area[-1])

    print(deltaQ[0])
    print(deltaQ[-1])

def main():
    args = interface.single()
    makeplot(args.figname, args.start, args.stop, args.step, args.time_unit)

if __name__ == '__main__':
    main()
