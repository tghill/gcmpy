"""

    interface.py

    Module provides a consistent and centralized command-line interface.

    Each plotting module should take the command line interface from here. If
    necessary, use the available functions to define new arguments and then
    parse.


    Functions:

     *  ArgumentParser: Return an argparse.ArgumentParser with the arguments
                            for either a single or comparison plot already added.
                            See below for arguments included.
     *  single:         Return a Namelist object with the arguments
                            for a single plot parsed
     *  multiple        Return a Namelist object with the arguments
                            for a comparison plot parsed

    _________________________________________________________________

    Arguments

    The "base" can be specified as 'single' or 'multiple'

    Arguments, depending on base, are:
    if base == 'single':

        * file            : Where to save the created figure
        * [--start START] : Starting index in list of files to plot
        * [--stop STOP]   : Ending index in list of files to plot
        * [--step STEP]   : Step size in passing through list of files
        * [--time_unit T] : Unit to measure time in: one of 'day', '1e3',
                            or (None or '')

    if base == 'multiple':
        Arguments from 'single' plus

        * dirs                : List of directories to plot data from
        * [--labels LABELS]   : Legend labels. Pass as many labels here as
                                directories in dirs
        * [--include_delta f] : Boolean flag to include a second subplot showing
                                the difference between the mean across two
                                model runs

"""

import argparse

# Strings to use as help strings when -h flag is used on command-line
help =  {   'file'  :   'Where to save the created figure',
            'dirs'  :   'Arbitrary number of directories to compare',
            'labels':   'Legend labels for lines corresponding to the data in dirs',
            'start' :   'Index in the list of available files to start plotting from',
            'stop'  :   'Index in the list of available files to plot until',
            'step'  :   'Step size when passing through list of files',
            'include_delta' :   'Include a second panel showing the difference between'
                                'two model runs. Plots the difference between the'
                                'first and last runs specified in dirs',
            't_unit':   'Unit to measure time in, one of'
                        '   *   "" to measure in s'
                        '   *   "1e3" to measure in thousands on s'
                        '   *   "day" to measure in days'
        }

def ArgumentParser(base = 'multiple'):
    """Return an argparse.ArgumentParser with default arguments pre-added.

    base can be either 'single' or 'multiple', the function will pre-load
    the corresponding command line arguments. This is useful to centralize
    the help strings and command line interface.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('file', help = help['file'])
    parser.add_argument('--start', help = help['start'], type = int, default = 0)
    parser.add_argument('--stop', help = help['stop'], type = int, default = None)
    parser.add_argument('--step', help = help['step'], type = int, default = 1)
    parser.add_argument('--time_unit', help = help['t_unit'], default = '')

    if base == 'multiple':
        parser.add_argument('--dirs', help = help['dirs'], nargs = '+')
        parser.add_argument('--labels', help = help['dirs'], nargs = '+')
        parser.add_argument('--include_delta', help = help['include_delta'], default = False)

    return parser

def single():
    """Return Namelist containing command line arguments.

    This function parses the arguments for a simple line plot, and returns
    the Namelist object with the argument values.

    Attributes:
     *  file
     *  start
     *  stop
     *  step
     *  time_unit
    """
    parser = ArgumentParser(base = 'single')
    args = parser.parse_args()
    return args


def multiple():
    """Return Namelist containing command line arguments.

    This function parses the arguments for a simple line plot, and returns
    the Namelist object with the argument values.

    Attributes:
     *  file
     *  start
     *  stop
     *  step
     *  time_unit
     *  dirs
     *  labels
    """
    parser = ArgumentParser(base = 'multiple')
    args = parser.parse_args()
    return args
