"""

    TKE.py

    Make a line plot of the total kinetic energy as a function of time.

    Kinetic energy is computed from U, V, and W fields as simply

    TKE = 0.5 * (U*U + V*V + W*W)

    Command line interface is standard interface from documentation

"""

import glob
import os
import argparse

import numpy as np
import netCDF4 as nc

import matplotlib
matplotlib.use('Agg')

from matplotlib import pyplot as plt

from . import modeldata
from . import find
from . import interface
from . import timeutils

rhoref = 1.E3

def main(figname, start, stop, step, time_unit):
    filedata = find.all('', start = start, stop = stop, step = step)
    files, iters = filedata.files, filedata.iters

    modelparams = modeldata.getdata()
    times = [int(k)*modelparams.deltaT for k in iters]

    tke = []

    for file in files:
        print("Plotting file %s" % file)
        data = nc.Dataset(file, 'r')
        u = np.array(data['U'])
        v = np.array(data['V'])
        w = np.array(data['W'])
        try:
            rho = rhoref + np.array(data['Rho'])
        except:
            rho = rhoref
        dx = modelparams.dx
        dy = modelparams.dy
        dz = modelparams.dz
        KE = 0.5 * np.sum(rho * (u*u + v*v + w*w) * (dx * dy * dz))
        tke.append(KE)

    times, xlab = timeutils.convert(times, time_unit)

    fig, ax = plt.subplots()

    ax.set_xlabel(xlab)
    ax.set_ylabel('Total Kinetic Energy (J)')

    ax.plot(times, tke)

    ax.set_title("Total Kinetic Energy")
    ax.grid()

    fig.savefig(figname, dpi = 600)

if __name__ == "__main__":
    args = interface.single()
    main(args.file, args.start, args.stop, args.step, args.time_unit)
