"""

    icehist.py

    Make line plot of total ice mass as a function of time

"""

import glob
import os
import argparse

import numpy as np
import netCDF4 as nc

import matplotlib
matplotlib.use('Agg')

from matplotlib import pyplot as plt

from . import modeldata
from . import find
from . import interface
from . import timeutils

rho_ice = 916.75    # ice density (kg / m^3)

def main(figname, start, stop, step, time_unit):
    filedata = find.all('', start = start, stop = stop, step = step)
    files = filedata.files
    iters = filedata.iters
    modelparams = modeldata.getdata()
    times = modelparams.deltaT * np.array(iters)
    times, xlab = timeutils.convert(times, time_unit)

    icemass = []

    for file in files:
        print("Plotting file %s" % file)
        data = nc.Dataset(file, 'r')
        heff = np.array(data['SIheff'])
        mass = np.sum(heff * modelparams.dx * modelparams.dy) * rho_ice
        icemass.append(mass)

    fig, ax = plt.subplots()

    ax.set_xlabel(xlab)
    ax.set_ylabel('Ice Mass (Kg)')

    ax.plot(times, icemass)

    ax.set_title("Total ice mass")
    ax.grid()

    fig.savefig(figname, dpi = 600)

if __name__ == "__main__":
    args = interface.single()
    main(args.file, args.start, args.stop, args.step, args.time_unit)
