"""
    reprocess.py

    Perform an extra post-processing step to expand any 2D datasets
    (like surface ice fraction) to 3D (with data only at the top level)
    for visualization purposes.

"""

import glob
import os
import argparse

import numpy as np
import netCDF4 as nc

from . import find

def main():
    """Convert any 2D data fields to 3D data fields.

    Sets all unused values to 0
    """
    filedata = find.all('')
    for infile in filedata.files:
        print('Regridded file', infile)
        outfile = infile.replace('.nc', '.regrid')
        indataset = nc.Dataset(infile, 'r')
        outdataset = nc.Dataset(outfile, 'w')
        Nx = indataset.dimensions['x'].size
        Ny = indataset.dimensions['y'].size
        Nz = indataset.dimensions['z'].size

        outdataset.createDimension('x', Nx)
        outdataset.createDimension('y', Ny)
        outdataset.createDimension('z', Nz)
        outdataset.createDimension('time', 1)
        for var in indataset.variables:
            data = indataset[var]
            if data.ndim == 2:
                outdata = np.zeros((Nz, Ny, Nx))
                outdata[0] = data[:]
                newvar = outdataset.createVariable(var, np.float64, ('z', 'y', 'x'))
                newvar[:, :, :] = outdata[:]

            elif data.ndim == 3:
                newvar = outdataset.createVariable(var, np.float64, ('z', 'y', 'x'))
                newvar[:, :, :] = data[:]

            elif data.ndim == 1:
                newvar = outdataset.createVariable(var, np.float64, data.dimensions)
                newvar[:] = data[:]
        indataset.close()
        outdataset.close()

if __name__ == '__main__':
    main()
