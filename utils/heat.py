"""

    heat.py

    Script to plot total change in heat/thermal energy.

    Change in heat is computed as

    Q = m * c * deltaTheta

    Where m is computed per grid cell from the density anomaly field (if it
    is available).

    Command line interface is standard interface defined in docs

"""

import argparse
import os

import numpy as np
import netCDF4 as nc

import matplotlib
matplotlib.use('Agg')

from matplotlib import pyplot as plt

import cmocean

from . import modeldata
from . import find
from . import interface
from . import timeutils

# Constant: specific heat capacity of water (J / (kg C) )
c = 4186.0

# Constant: Density of water at 4C
rhoref = 1.e3

dir = ''
def makeplot(figname, start = 0, step = 1, stop = None, time_unit = ''):
    filedata = find.all(dir, start = start, step = step, stop = stop)
    mp = modeldata.getdata(data = os.path.join(dir, 'data'),
                                  datapkg = os.path.join(dir, 'data.pkg'))
    nfiles = len(filedata.files)
    shape = (nfiles,)
    deltaQ = np.zeros(shape)

    data0 = nc.Dataset(filedata.files[0])
    T0 = np.array(data0['T'])
    for j,file in enumerate(filedata.files[1:]):
        print("Plotting file %s" % file)
        data = nc.Dataset(file)
        T1 = np.array(data['T'])
        dT = T1 - T0

        try:
            rho = rhoref + np.array(data['Rho'])
        except:
            rho = rhoref

        deltaQ[j+1] = c * np.sum(rho * mp.dx * mp.dy * mp.dz * dT)

    times = mp.deltaT * np.array(filedata.iters)
    times, xlab = timeutils.convert(times, time_units)
    
    fig, ax = plt.subplots()
    qp = ax.plot(times, deltaQ)
    ax.grid()
    ax.set_xlabel(xlab)
    ax.set_ylabel('Q (J)')
    ax.set_title('Internal energy of water')
    fig.savefig(figname, dpi = 600)

def main():
    args = interface.single()
    makeplot(args.file, args.start, args.stop, args.step, args.time_unit)

if __name__ == '__main__':
    main()
