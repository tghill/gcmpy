"""

average2d plots the mean value of a given variable. Since you must specify the
variable, the command line interface has more options. The given variable
is averaged in all dimensions at each time step, and the mean value
is plotted as a function of time.

Command Line Interface:

    python -m gcmpy.utils.average2d figname field [--title --ylabel --step --start --stop --time_unit]

Where
 * figname is what to save the image as
 * field specifies the variable to average
 * title and ylabel specify what to label the plot with
 * start, step, stop specify which iterations to plot. The list of files and
    indices is indexed from start to stop, in stepsize step.
      files[start:stop:step]
 * time_unit is one of ('day', '1e3', or None or ''). 'day' plots x axis in
    days, '1e3' plots x axis in 1000 seconds, and none or '' plots in seconds

"""

import glob
import os
import argparse

import numpy as np
import netCDF4 as nc

import matplotlib
matplotlib.use('Agg')

from matplotlib import pyplot as plt

from . import modeldata
from . import find
from . import interface
from . import timeutils

rho_ice = 916.75    # ice density (kg / m^3)

# fill value. Averages taken where data != fill
fill = -999.

def main(figname, field, ylabel, title, start = 0, stop = None, step = 1, tunit = ''):
    filedata = find.all('', step = step, start = start, stop = stop)
    modelparams = modeldata.getdata()
    times = modelparams.deltaT * np.array(filedata.iters)

    avg = []

    for file in filedata.files:
        print("Plotting file %s" % file)
        data = nc.Dataset(file, 'r')
        arr = data[field][:]
        avg.append(arr[arr!=fill].mean())

    times, xlab = timeutils.convert(times, tunit)

    fig, ax = plt.subplots()

    ax.set_xlabel(xlab)
    ax.set_ylabel(ylabel)

    ax.plot(times, avg)

    ax.set_title(title)
    ax.grid()

    fig.savefig(figname, dpi = 600)

if __name__ == "__main__":
    parser = interface.ArgumentParser()
    parser.add_argument("--title", default = '')
    parser.add_argument('--ylabel', default = '')
    parser.add_argument('field', help = 'Data field to average')
    args = parser.parse_args()
    main(args.file, args.field, args.ylabel, args.title, args.start, args.stop, args.step, args.time_unit)
