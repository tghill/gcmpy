"""

    deltaIce.py

    Compare the total ice mass between different model runs.

    Interface is the standard multiple-run interface defined in interface.py
    and in the documentation.

"""

import os

import numpy as np
import netCDF4 as nc

import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt

from . import modeldata
from . import find
from . import interface
from . import timeutils

rho_ice = 916.75    # ice density (kg / m^3)

def main(figname, dirs, labels, start, stop, step, include_delta = False, time_unit = ''):
    filedata = [find.all(dir, step = step, start = start, stop = stop) for dir in dirs]
    mp = [modeldata.getdata(data = os.path.join(dir, 'data'), datapkg = os.path.join(dir, 'data.pkg')) for dir in dirs]

    nfiles = len(filedata[0].files)

    icemass = [np.zeros(np.shape(fd.iters)) for fd in filedata]

    for j in range(len(icemass)):
        for i in range(len(filedata[j].files)):
          if j == 0:
            print('Plotting file %s' % filedata[j].files[i])
          data = nc.Dataset(os.path.join(dirs[j], filedata[j].files[i]), 'r')
          try:
            heff = np.array(data['SIheff'])
          except:
            heff = np.nan
          mass = np.sum(rho_ice * heff * mp[j].dx * mp[j].dy)
          icemass[j][i] = mass

    times = [mp[j].deltaT * np.array(fd.iters)/tscale for j,fd in enumerate(filedata)]
    times, xlab = timeutils.convert(times, timeunit)

    if include_delta:
        fig, (ax1, ax2) = plt.subplots(figsize = (6, 6), sharex = True, nrows = 2)
        ax2.plot(times[0], icemass[-1] - icemass[0])
        ax2.grid()
        ax2.set_xlabel(xlab)
        ax2.set_ylabel('Delta mass (Kg)')
        ax2.set_title('Delta ice mass')

    else:
        fig, ax1 = plt.subplots()
        ax1.set_xlabel(xlab)

    for j, label in enumerate(labels):
        ax1.plot(times[j], icemass[j], label = label)

    ax1.grid()
    ax1.legend()

    ax1.set_ylabel('Mass (Kg)')
    ax1.set_title('Total ice mass')

    plt.tight_layout()

    fig.savefig(figname, dpi = 600)

if __name__ == '__main__':
    args = interface.multiple()
    main(args.file, args.dirs, args.labels, args.start, args.stop, args.step,
                                include_delta = args.include_delta)
