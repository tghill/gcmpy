"""

    deltaTKE.py

    Compare the total kinetic energy between model runs.

    Uses the standard command line interface defined in interface.py and
    in the documentation

"""

import glob
import os
import argparse

import numpy as np
import netCDF4 as nc

import matplotlib
matplotlib.use('Agg')

from matplotlib import pyplot as plt

from . import modeldata
from . import find
from . import interface
from . import timeutils

rhoref = 1.E3

def main(figname, dirs, labels, start, stop, step, include_delta = True, timeunit = ''):
    filedata = [find.all(dir, step = step, start = start, stop = stop) for dir in dirs]
    mp = [modeldata.getdata(data=os.path.join(dir, 'data'),
            datapkg = os.path.join(dir, 'data.pkg')) for dir in dirs]

    times = [mp[j].deltaT * np.array(fd.iters)/1.e3 for j,fd in enumerate(filedata)]

    all_tke = [np.zeros(np.shape(fd.iters)) for fd in filedata]

    for j in range(len(all_tke)):
        for i in range(len(filedata[j].files)):
            print('Plotting file %s' % os.path.join(dirs[j], filedata[j].files[i]))
            data = nc.Dataset(os.path.join(dirs[j], filedata[j].files[i]), 'r')
            try:
                u = np.array(data['U'])
                v = np.array(data['V'])
                w = np.array(data['W'])
            except:
                all_tke[j][i] = all_tke[j][i-1]
                continue

            try:
                rho = rhoref + np.array(data['Rho'])
            except:
                rho = rhoref

            dx = mp[j].dx
            dy = mp[j].dy
            dz = mp[j].dz
            KE = 0.5 * np.sum(rho * (u*u + v*v + w*w) * (dx * dy * dz))
            KE = KE if KE <= 1.e50 else np.nan
            all_tke[j][i] = KE

    times, xlab = timeutils.convert(times, timeunit)
    if include_delta:
        fig, (ax1, ax2) = plt.subplots(nrows = 2, sharex = True, figsize = (6, 6))
        ax2.set_xlabel(xlab)
        ax2.set_ylabel('Delta Kinetic Energy (J)')
        ax2.plot(times[0], all_tke[-1] - all_tke[0])
        ax2.grid()
        ax2.set_title('Delta Kinetic Energy')
    else:
        fig, ax1 = plt.subplots()
        ax1.set_xlabel(xlab)

    ax1.set_ylabel('Kinetic Energy (J)')
    ax1.set_title('Total Kinetic Energy')

    for j,row in enumerate(all_tke):
        ax1.plot(times[j], row, label = labels[j])

    ax1.legend()
    ax1.grid()

    plt.tight_layout()
    fig.savefig(figname, dpi = 600)

if __name__ == "__main__":
    args = interface.multiple()
    main(args.file, args.dirs, args.labels, args.start, args.stop, args.step,
                                    include_delta = args.include_delta)
