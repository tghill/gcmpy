# Usage

The post-processing routines are run by passing a specification file as a command line argument to the python package. Since `postprocessing` is part of the larger package `gcmpy`, call the module name as `gcmpy.postprocessing`.

```bash
python -m gcmpy.postprocessing gifargs.py
```

where `gifargs.py` provides all the information necessary to make the figures.

## Specification file

The specification file has three important variables which specify how to make the figures. They are given in the table below.

Name            |   Description
--------------- | ---------------------
GLOBAL_GIF_ARGS | Dictionary specifying global arguments applying to all figures
ARGS3D          | List of dictionaries specifying arguments for each 3D figure
ARGS2D          | List of dictionaries specifying arguments for each 2D figure

Be sure to check the spelling of these variables, since the package specifically looks for these names.

The file will probably look something like

```python
GLOBAL_GIF_ARGS = { }

ARGS3D = [
             { },
             { }
         ]

ARGS2D = [
            { },
            { }
        ]
```
Where each of the dictionaries will have keys/values according to the available parameters.

The specification file can be in any directory. The intended usage pattern is to make the specification file in the simulation directory where it is probably most convenient. See some examples in the `postprocessing/examples` directory. The figures in the `examples` directory were made using the example `makefigures.py`.

# Available Parameters
The figures are highly customizable through the parameters passed in the specification file. Plots for 3D and 2D datasets have slightly different parameters available to use.

## 3D Datasets
These are the parameters available for plotting 3D datasets (ie. T Rho, U, V). They are split into required parameters and optional parameters.

### Required:

Name        |   Description
----        |   ------------------
var         |   Variable prefix to plot. This is the prefix of the MITgcm binary output files. eg 'T', 'Rho'.
movie_name  |   Filename to save the gif animation. This works with or without the .gif. extension
cut_var     |   Axis to take a constant slice of. One of 'x', 'y' or 'z'.
cut_val     |   Value to take a slice at. Must be a level in the model data.

### Optional:

Name                |   Default     |   Description
--------------------|---------------|--------------------
data_path           |   '.'         |   Path (absolute or relative) to the 'data' model configuration file. Uses 'data' file for deltaT and startTime fields.
iters               |   None        |   If None, use all iterations for matching file names. Otherwise,  can be a list of iteration numbers. Iterations are auto zero padded to 10 digits
vmin                |   None (auto) |   Colour scale min
vmax                |   None (auto) |   Colour scale max
image_folder_name   |   PNG_IMAGES  |   Directory to save image files in
gif_folder_name     |   GIF_IMAGES  |   Directory to save animation in
image_name          |   None        |   Name to save images as if given. The model iteration number is put before the file extension. If left as None, does not save each still frame.
namespec            |   output_{iter}.nc    |   Specifies a file name pattern for the .nc files
fps                 |   2           |   Frames per second in the output .gif animation
cmap                |'cmocean/thermal'|   Colour map for animation. You will have to change this if you don't have the cmocean package.
dpi                 |   200         |   Resolution for still frames
plot_type           |   'gs'        |   One of None, 'gs', 'contour' or 'interp'. None: pcolormesh with no shading/interpolation. gs: pcolormesh with gouraud shading. interp: imshow with interpolation.
interp_type         |   'bilinear'  |   Interpolation type. See [pyplotimshow documentation](https://matplotlib.org/api/_as_gen/matplotlib.pyplot.imshow.html)
aspect              |   'auto'      |   One of 'auto' or number. Auto uses a 4:3 aspect ratio; Passing a number forces that aspect ratio
velocity_field      |   False       |   Flag to show the direction field overtop of the pcolor plot. Requires fields 'U' and 'V' to be in the netCDF file.
stride              |   3           |   stride length for picking data to plot for ice velocity field. ie, if stride = 3, only shows every an arrow for every 3rd data point. If stride is given, the same stride will be used for x, y, and z directions. Otherwise, can specify individually xstride, ystride, and zstride.
xstride             |   None        |   stride length in x for picking data to plot for ice velocity field. ie, if stride = 3, only shows every an arrow for every 3rd data point. If given, will override the 'stride' value for the x direction only.
ystride             |   None        |   stride length in y for picking data to plot for ice velocity field. ie, if stride = 3, only shows every an arrow for every 3rd data point. If given, will override the 'stride' value for the y direction only.
zstride             |   None        |   stride length in z for picking data to plot for ice velocity field. ie, if stride = 3, only shows every an arrow for every 3rd data point. If given, will override the 'stride' value for the z direction only.
scale    
scale               |   20          |   Scale for ice velocity field arrow size. A larger scale means smaller arrows; smaller scale means larger arrows.


## 2D Datasets
These are the parameters available for plotting 2D datasets (eg. sea ice area fraction). They are split into required parameters and optional parameters.

### Required:

Name        |   Description
------------|-------------------
var         |   Variable prefix to plot. This is the prefix of the MITgcm binary output files. eg 'T', 'Rho'.
movie_name  |   Filename to save the gif animation. This works with or without the .gif. extensionr specific  set of iterations)
sec_per_iter|   Seconds per model iteration.

Optional:

Name                |   Default       |   Description
--------------------|---------------  |--------------------
data_path           |   '.'           |   Path (absolute or relative) to the 'data' model configuration file. Uses 'data' file for deltaT and startTime fields.
iters               |   None          |   If None, use all iterations for matching file names. Otherwise, can be a list of iteration numbers. Iterations are auto zero padded to 10 digits
vmin                |   None (auto)   |   Colour scale min
vmax                |   None (auto)   |   Colour scale max
image_folder_name   |   PNG_IMAGES    |   Directory to save image files in
gif_folder_name     |   GIF_IMAGES    |   Directory to save animation in
image_name          |   None          |   Name to save images as if given. The model iteration number is put before the file extension. If left as None, does not save each still frame.
namespec            | output_{iter}.nc|   Specifies a file name pattern for the .nc files
fps                 |   2             |   Frames per second in the output .gif animation
cmap                |   'cmocean/ice' |   Colour map for animation. Colourmap ice from cmocean package. If you don't have cmocean you will have to change this each time.
dpi                 |   200           |   Resolution for still frames
plot_type           |   'gs'          |   One of None, 'gs', 'contour' or 'interp'. None: pcolormesh with no shading/interpolation. gs: pcolormesh with gouraud shading. interp: imshow with interpolation
interp_type         |   'bilinear'    |   Interpolation type. See [pyplot imshow documentation]( https://matplotlib.org/api/_as_gen/matplotlib.pyplot.imshow.html)
aspect              |   'auto'        |   One of 'auto' or number. Auto uses a 4:3 aspect ratio; Passing a number forces that aspect ratio
ice_velocity_field  |   True          |   Overlay a direction field ofthe ice velocity (from UICE and VICE)
stride              |   3             |   stride length for picking data to splot for ice velocity field. ie, if stride = 3, only shows every an arrow for every 3rd data point
scale               |   20            |   Scale for ice velocity field arrow size. A larger scale means smaller arrows; smaller scale means larger arrows.
