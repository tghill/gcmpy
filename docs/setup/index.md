# MITgcmIce Documentation

This documentation is a guide to setting up the MITgcm in a configuration with ice. [GitHub page](https://github.com/timghill/gfg).

The essentials of running the model with ice are to enable the `EXF` package and one or both of the `SEAICE` and `THSICE` packages. These packages are described in detail in their respective sections.
The data files for a working model run (working with [checkpoint 67a](https://github.com/MITgcm/MITgcm/releases/tag/checkpoint67a) ) are in the code repository
