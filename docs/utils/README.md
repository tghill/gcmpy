# README for `gcmpy/utils`
This directory contains some helpful modules for making figures from MITgcm converted netCDF output, as well as some standalone scripts to make diagnostic plots.

## Helper Modules

### `modeldata.py`
Reads the model input namelists, and returns a python object containing the data. The function `getdata` takes the paths to the `data` and `data.pkg` files, and returns a namedtuple object with attributes corresponding to the input namelists.

Call signature:

```python
getdata(data = 'data', datapkg = 'data.pkg', datadiagnostics = None):
```

Arguments:

 * `data`: Path to `data` file the model was run from
 * `datapkg`: Path to `data.pkg` file the model was run from
 * `datadiagnostics`: Path to `data.diagnostics` file (if used).

Returns:

 * Named Tuple with attributes `dx`, `dy`, `dz`, `nx`, `ny`, `nz`, `startTime`, `endTime`, `deltaT` `delX`, `delY`, `delZ`.

### `find.py`
Function `find.all` finds all netCDF files in a given directory, and returns a list of files sorted by iteration. It slices the list as `files[start:stop:step]`, where the  `start`, `stop`, `step` arguments are optional keyword arguments.

Positional Arguments:

 * `dir`: Directory the netCDF files are stored in

Keyword Arguments with defaults:

 * `start = 0`: Index in the list of files to start with
 * `stop = None`: Index in the list of files to stop with. If None, goes to the end. Otherwise if it is an integer, slices until that index.
 * `step = 1`: Integer step size when slicing list of files.

Returns:

 * `filedata`: a namedtuple with attributes `files` and `iters`. `filedata.files` is a sorted list of file names (with the directory stripped), and `fieldata.iters` is the corresponding sorted list of iterations.

Example:

```python
filedata = find.all('run', start = 1, stop = 20, step = 2)
files = filedata.files
iters = filedata.iters
```

Searches in the directory `run/` for all `*.nc` files. It returns the order tuple which is assigned to `filedata`, which are accessible with `.files` and `.iters` attributes. In this case, it will contain files and iteration numbers for the 2nd, 4th, ..., 18th files in the directory.


### `interface.py`
Defines a common, centralized, and consistent command-line interface for most figures. The interface is defined for both "single-run" plots and "multiple-run" plots, depending on if you're looking at data from one parameter set or comparing data from multiple different model runs.

### `reprocess.py`
Copy netCDF files and modify and 2D fields to 3D datasets, padded with 0's. When doing 3D visualization and trying to plot ice fraction/thickness and a 3D field (Temperature, desntiy, etc) you need the arrays to be the same dimension. So, this module pads the 2D array underneath with 0s for other levels. This lets you visualize both fields at once. Saves files with the same name, but ending `.regird`.

#### Single-run interface
The single-run interface is the function `interface.single()`, which takes no arguments. The command line interface specified is

```python
python -m gcmpy.utils.[module_name] file [--start START] [--stop STOP] [--step STEP] [--time_unit T]
```

Where the arguments are:

* file            : Where to save the created figure
* [--start START] : Starting index in list of files to plot
* [--stop STOP]   : Ending index in list of files to plot
* [--step STEP]   : Step size in passing through list of files
* [--time_unit T] : Unit to measure time in: one of 'day', '1e3', or (None or '')

#### Multiple-run -interface
The comparison interface is the function `interface.multiple()`, which also takes no arguments. The command line interface specified is

```python
python -m gcmpy.utils.[module_name] file dirs --labels LABELS [--start START] [--stop STOP] [--step STEP] [--time_unit T]
```

Where the arguments are:

* file            : Where to save the created figure
* dirs                : List of directories to plot data from
* --labels LABELS   : Legend labels. Pass as many labels here as directories in dirs
* [--start START] : Starting index in list of files to plot
* [--stop STOP]   : Ending index in list of files to plot
* [--step STEP]   : Step size in passing through list of files
* [--time_unit T] : Unit to measure time in: one of 'day', '1e3', or (None or '')

#### Other interface
You can define your own argument parser with the function `interface.ArgumentParser(base = 'multiple')`. Specify base as `'multiple'` or `'single'`, and it will return an `argparse.ArgumentParser` object with the corresponding args already added. You can then add arguments from there. If you need fewer arguments, just start from the base `argparse.ArgumentParser()` object.


## `humidity.py`
Provides functions for converting between relative humidity and specific humidity.

Function `q(T, P, RH)` returns the specific humidity corresponding to relative humidity `RH` at temperature and pressure `T` and `P`.

Function `RH(T, P, q)` returns the relative humidity corresponding to specific humidity `q` at temperature and pressure `T` and `P`.

## `timeutils.py`
Provide function for converting time from seconds to days, 10^3 seconds, or keep in seconds, and formatting time axis labels with the correct units.

# Available plots
Most of the available plots are simple line plots, showing an average or otherwise computed quantity evolving over time. There are options for plotting quantities from a single model run, or to compare the same quantity over multiple runs. There are some additional plots, with separate descriptions given.


## Single run line plots
The line plots here show the value of one parameter or computed quantity over time. The command line interface is as consistent as possible

```bash
python -m gcmpy.utils.[module] figname [--start START] [--stop STOP] [--step STEP]
```

Where

 * `figname` is where to save the resulting image
 * `START`, `STOP`, and `STEP` specify the iterations to take. The list of all files is sliced starting at START until STOP, with a step size STEP

Some modules may take additional arguments. Available modules are listed below

Module name     |   Description                     | Extra arguments
--------------- | --------------------------------- | -------------------------
average2d       | Plot the spatial mean of an arbitrary specified field. Requires extra arguments to define which field to plot, the title, and the ylabel  | `field [--title TITLE] [--ylabel YLABEL]`
heat            | Plot total internal energy. Computed using initial conditions as reference state |
icehist         | Total ice mass computed from effective height, grid size, and standard ice density |
TKE             | Total kinetic energy |
tprof           | Make an *animated* line plot showing the temperature vertical profile over time | `[--still STILL]` optionally save still frames with name `STILL`
tsurf           | Plot the mean surface temperature over time |

## Comparison line plots
The idea behind the comparison plots is to compare data between model runs with slightly different parameters. The default behaviour is to overlay the data from all the model runs onto the same plot. Another panel can be optionally added to show to difference between two cases. This is especially useful if only two parameter sets are being considered.

The plots follow a uniform command line interface as much as possible. The interface is

```
python -m gcmpy.utils.delta[plot] figname [--dirs DIRS ...]
                                          [--labels LABELS ... ]
                                          [--start START] [--stop STOP]
                                          [--step STEP] [--include_delta]
                                          [additional options]
```

Where

 * `DIRS` lists all the directories to compare (at least 1 must be given)
 * `LABELS` specifies the legend labels for the plot
 * `START`, `STOP`, and `STEP` specify the iterations to take. The list of all files is sliced starting at START until STOP, with a step size STEP.
 * `--include_delta` is an option to include another panel with the difference between two cases. The difference is taken between the last - first directories passed. This is mostly useful when only two cases are being compared.

Some plots (specifically `deltaAverage2d`) have additional options.

The following table lists available plots

Module name     |   Description                     | Extra arguments
--------------- | --------------------------------- | -------------------------
deltaAverage2d  | Compare the mean of an arbitrary specified field  | `[--title TITLE] [--ylabel YLABEL]`
deltaIce        | Compare total ice mass, computed from effective height and density. |
deltaQ          | Compare total internal energy, using initial conditions as reference state |
deltaTKE        | Compare total kinetic energy |
deltaHeff       | Compare mean ice thickness |

## Additional Plots
The plots listed here aren't line plots, but can still be useful.

### tcolour
Plot a [Hovmoller diagram](https://en.wikipedia.org/wiki/Hovmöller_diagram) of horizontally-averaged temperature profile over time. Z is on the y-axis, and time on the x-axis. The plot is coloured by temperature. The command line interface is consistent iwth the above interfaces

```
python -m gcmpy.utils.tcolour figname [--start --stop --step]
```

With the same definitions as usual

### fluxes
Make figure with many panels (currently 9) relating energies and heat fluxes. Subfigures plotted are (left to right across rows):

  1.     Rate of change of total internal energy of water
  2.     Rate of change of total internal energy of water per unit area
  3.     SIqnet: Mean total heat flux out of the water surface. For ice covered areas, flux is water to ice. For open water, flux is water to atmosphere.
  4.     SIatmQnt: Total atmospheric heat flux. For ice covered areas, heat flux from ice to atmosphere. For open water, heat flux from ocean to atmosphere.
  5.     Ice covered part of SIqnet. Total heat flux from water into ice, averaged over the whole domain.
  6.    Ice covered part of SIatmQnt. Total heat flux from ice into atmosphere, averaged over the whole domain.
  7.     Rate of change of total internal energy of water per unit area + net heat flux out of the water. This should theoretically be identically zero, but the rate of change plotted in (2) is computed numerically as an average between files. Some small numerical error is expected.
  8.    EXFhs:  Sensible heat flux into water, positive increases temperature
  9.    EXFhl:  Latent heat flux into water, positive increases temperature
