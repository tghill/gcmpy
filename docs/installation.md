# Installation
Clone or download and extract the code. Put the code directory on your python path, or add the directory to your python path by adding the following line to your `.bashrc` file

```bash
export PYTHONPATH="${PYTHONPATH}:/path/to/package"
```

# Requirements

## Python version
Becuase of matplotlib, python version >= 3.4 is required. The core modules have been written for python 3 only. Python 2.x is not supported.

## Python Packages
gcmpy requires the following python packages

* numpy
* [netCDF4](http://unidata.github.io/netcdf4-python/), which requires the netCDF and HDF5 C libraries.
* [MITgcmutils](https://github.com/MITgcm/MITgcm), install from the folder `$MITgcm/utils/python/MITgcmutils` and install with `python3 setup.py install`.
    * If you aren't using [virtualenv](https://virtualenv.pypa.io/en/stable/) for package managment you will likely need to specify `--prefix={where/to/install}`, again adding the path to your python path (or installing into your local python path probably in `~/.local/`) See note below if this doesn't work.
 * [f90nml](https://github.com/marshallward/f90nml) to parse Fortran namelists
 * [matplotlib](https://matplotlib.org)

**Optional Packages**

 * [cmocean](https://github.com/matplotlib/cmocean) for colourmaps (available on package index)

## Installing MITgcmutils
Note: Sometimes installing MITgcmutils can be harder than expected. If you have an error like

```
Checking .pth file support in /u/tghill/.local/lib/python3.4/site-packages/
/u/tghill/pyenv/bin/python3 -E -c pass
TEST FAILED: /u/tghill/.local/lib/python3.4/site-packages/ does NOT support .pth files
error: bad install directory or PYTHONPATH

You are attempting to install a package to a directory that is not
on PYTHONPATH and which Python does not read ".pth" files from.  The
installation directory you specified (via --install-dir, --prefix, or
the distutils default setting) was:

    /u/tghill/.local/lib/python3.4/site-packages/

and your PYTHONPATH environment variable currently contains:

    '/usr/lib64/python2.6/site-packages/openmpi-1.10:/u/tghill/pkg'

Here are some of your options for correcting the problem:

* You can choose a different installation directory, i.e., one that is
  on PYTHONPATH or supports .pth files

* You can add the installation directory to the PYTHONPATH environment
  variable.  (It must then also be on PYTHONPATH whenever you run
  Python and want to use the package(s) you are installing.)

* You can set up the installation directory to support ".pth" files by
  using one of the approaches described here:

  https://setuptools.readthedocs.io/en/latest/easy_install.html#custom-installation-locations


Please make the appropriate changes for your system and try again.
```

Then try adding the given path (in this case `/u/tghill/.local/lib/python3.4/site-packages/`) to your python path by adding the following line to your ~/.bashrc file

```bash
export PYTHONPATH="${PYTHONPATH}:/u/tghill/.local/lib/python3.4/site-packages/"
```

Then reload your rc file by running `source ~/.bashrc`, and try installing again:

```bash
python3 setup.py install --prefix ~/.local
```

This has worked for me on Hood.

On Graham, you might also have to add to your python path, but is has worked for me:

```bash
module load python/3.5.4
python3 setup.py install --prefix ~/.local
```
