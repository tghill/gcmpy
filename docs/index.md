# Welcome to gcmpy documentation
This is the home documentation page of MITgcm python processing routines. This documentation and code live on [GitLab]([https://git.uwaterloo.ca/tghill/gcmpy)

This package consists of a few independent python packages bundled together and corresponding documentation.

Along with this documentation, some more useful resources are:

 * [UW fluids group wiki](https://wiki.math.uwaterloo.ca/fluidswiki/index.php?title=Main_Page)
 * The MITgcm [GitHub repository](https://github.com/MITgcm/MITgcm) and [Documentation](http://mitgcm.readthedocs.io/en/latest/)
 * [MITgcm tutorials](https://wiki.math.uwaterloo.ca/fluidswiki/index.php?title=MITgcm_Tutorials) on the group wiki page

For more documentation about MITgcm, see the pdf documentation under the `docs/` folder on GitHub.
