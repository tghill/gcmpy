# netCDFbinary
netCDFbinary is a python package for converting MITgcm binary model output `.data` files to NetCDF `.nc` files. The variables to be included in the netcdf files are specified as command line arguments.

The core functionality of this package comes from the [UW fluids group wiki](https://wiki.math.uwaterloo.ca/fluidswiki/index.php?title=NetCDF_Converter). That script was modified to work with 2D variables such as ice fraction (`ice_fract`) and wrapped in a more distributable package with a command line interface.
