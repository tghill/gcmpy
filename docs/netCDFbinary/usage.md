# Usage
The package is set up with a `__main__.py` file, to [run the package as a script](https://docs.python.org/2/using/cmdline.html) (`-m` flag). Since this is part of the larger package `gcmpy`, call the module name as `gcmpy.netCDFbinary`. See the examples below. Command line interface is

```bash
python -m gcmpy.netCDFbinary FIELDS [ITERS] [-o] [--expand2d]
```

- fields: The MITgcm variables to add to the netCDF file. These are the variable prefixes to the `.meta` and `.data` binary files. For example, `T` and `Rho`.
- [iters, optional]: If iters is specified, it is a list of iterations to convert. If it is not given, all available iterations will be converted.
- [-o, --overwrite]: Flag specifying whether to overwrite previously converted files. If given, all old netCDF files will be overwritten with new files. If not given, will skip any existing files.
- [--expand2d]: Flag specifying whether to pad 2D data fields with layers of 0s in a third dimension. This is useful if you will be using 3D visualization software (in particular VisIt) for graphics.

For example, to convert Temperature `T`, density `Rho`, and U and V velocities `U`, `V` for all available iterations:
```bash
python -m gcmpy.netCDFbinary T Rho U V
```

For the same fields but only select iterations, for example

```bash
python -m gcmpy.netCDFbinary T Rho U V --iters 0 10 20 30 40 50
```

And to overwrite any existing files

```bash
python -m gcmpy.netCDFbinary T Rho U V -o
```
